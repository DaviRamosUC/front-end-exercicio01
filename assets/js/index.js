var links = document.querySelectorAll(".menu");
for (var i = 0; i < links.length; i++) {
  links[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}

function trocaImagem(event) {
  let id = event.target.id;
  event.target.src = `./assets/img/${id}_hover.png`;
  event.target.addEventListener("mouseout", function () {
    event.target.src = `./assets/img/${id}.png`;
  });
}

